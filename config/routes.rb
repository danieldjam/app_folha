Rails.application.routes.draw do
  namespace :login do
    get 'welcome/index'
    post 'welcome/index'
  end
  namespace :site do
    get 'welcome/index'
    post 'welcome/index'
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
 
  root to: 'login/welcome#index'

end
